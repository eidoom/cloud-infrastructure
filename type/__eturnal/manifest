#!/bin/sh -e

os=$(cat "$__global/explorer/os")

case "$os" in
    debian)
        :
        ;;
    *)
        echo "OS $os currently not supported by type ${__type##*/}" >&2
        exit 1
        ;;
esac

secret="$(cat "$__object/parameter/secret")"
port="$(cat "$__object/parameter/port")"

__extrepo eturnal

require="__extrepo/eturnal" \
    __apt_update_again for_extrepo

require="__apt_update_again/for_extrepo" \
    __package eturnal

require="__package/eturnal" \
    __file "/etc/eturnal.yml" \
    --source - \
    --owner "root" \
    --group "eturnal" \
    --mode 0640 \
    << EOF
# eturnal STUN/TURN server configuration file.
#
# This file is written in YAML. The YAML format is indentation-sensitive, please
# MAKE SURE YOU INDENT CORRECTLY.
#
# See: https://eturnal.net/documentation/#Global_Configuration

eturnal:

  ## Shared secret for deriving temporary TURN credentials (default: \$RANDOM):
  secret: "$secret"

  ## The server's public IPv4 address (default: autodetected):
  #relay_ipv4_addr: "203.0.113.4"
  ## The server's public IPv6 address (optional):
  #relay_ipv6_addr: "2001:db8::4"

  listen:
    -
      ip: "::"
      port: $port
      transport: udp
    -
      ip: "::"
      port: $port
      transport: tcp
    #-
    #  ip: "::"
    #  port: 5349
    #  transport: tls

  ## TLS certificate/key files (must be readable by 'eturnal' user!):
  #tls_crt_file: /etc/eturnal/tls/crt.pem
  #tls_key_file: /etc/eturnal/tls/key.pem

  ## UDP relay port range (usually, several ports per A/V call are required):
  relay_min_port: 49152     # This is the default.
  relay_max_port: 65535     # This is the default.

  ## Reject TURN relaying from/to these addresses/networks:
  blacklist:                # This is the default blacklist.
    - "127.0.0.0/8"         # IPv4 loopback.
    - "::1"                 # IPv6 loopback.

  ## If 'true', close established calls on expiry of temporary TURN credentials:
  strict_expiry: false      # This is the default.

  ## Logging configuration:
  log_level: info           # critical | error | warning | notice | info | debug
  log_rotate_size: 10485760 # 10 MiB (default: unlimited, i.e., no rotation).
  log_rotate_count: 10      # Keep 10 rotated log files.
  #log_dir: stdout          # Enable for logging to the terminal/journal.

  ## See: https://eturnal.net/documentation/#Module_Configuration
  modules:
    mod_log_stun: {}        # Log STUN queries (in addition to TURN sessions).
EOF

require="__package/eturnal" \
    __systemd_unit eturnal.service \
    --enablement-state enabled \
    --restart

require="__file/etc/eturnal.yml" \
    __systemd_service eturnal \
    --action reload \
    --if-required
